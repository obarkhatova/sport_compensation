import json
import requests
import http.client
import urllib.parse
import urllib3
from attrdict import AttrMap


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
http.client.HTTPConnection.debuglevel = 3

class Session(requests.Session):
    # Status codes
    OK = 200
    CREATED = 201
    ACCEPTED = 202
    NO_CONTENT = 204
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    NOT_FOUND = 404
    INTERNAL_SERVER_ERROR = 500

    def __init__(self, host, location, protocol='https', timeout=5, verify=False, resp_format='brief'):

        super().__init__()
        self.timeout = timeout
        self.verify = verify
        # todo
        # you can manually set the Response.encoding property,
        # or use the raw Response.content."

        self.protocol = protocol
        self.host = host
        self.location = location
        self.route_start = ''
        self.X_CSRFToken = ''
        self.Referer = f'{protocol}://{host}/'
        self.url_param_separator = '?'
        self.urlencode_safe = ''
        self.resp_format = resp_format

        self.headers.update({'Content-Type': 'application/json'})

    def url(self, *args, **kwargs):
        """
            Returns url value
            args:  list of url chunk names
            kwargs[url_param]: url parameters
        """
        def url_concat(*chunks, **param):
            ch = lambda x: f'/{x}' if x else ''
            chs = ''.join(list(ch(chunk) for chunk in chunks))
            empty_keys = [key for key, val in param.items() if not val]
            for key in empty_keys:
                param.pop(key)
            url_param = (lambda x: f'{self.url_param_separator}{x}' if x else '') \
                (urllib.parse.urlencode(param, safe=self.urlencode_safe))
            return f'{self.protocol}://{self.host}{ch(self.location)}{chs}' \
                   f'{url_param}'

        if isinstance(args, str):
            args = [args]
        return url_concat(*args, **kwargs.get('url_param', {}))

    @property
    def resp_format(self):
        return self.__resp_format

    @resp_format.setter
    def resp_format(self, fmt):
        self.__formats = {'asis': self.asis_format,
                          'brief': self.brief_format,
                          'extended': self.extended_format
                          }
        try:
            self.format = self.__formats[fmt]
            self.__resp_format = fmt
        except KeyError:
            self.__resp_format = 'asis'
            self.format = self.__formats['asis']


    def asis_format(self, response):
        return response


    def extended_format(self, response):
        try:
            reason = json.loads(response.reason)
        except json.JSONDecodeError:
            reason = response.reason
        if isinstance(reason, list) and len(reason) > 0:
            reason = reason[0]

        data = AttrMap(sequence_type=None)
        data.update(
            status_code=response.status_code,
            reason=reason,
            content=self.__decode(response.content,
                                response.headers.get('Content-Type', '')),
            text=response.text,
            request={
                'url': response.request.url,
                'path_url': response.request.path_url,
                'method': response.request.method,
                'body': response.request.body,
            }
        )
        return response.status_code, reason, data


    def brief_format(self, response):
        return response.status_code, \
               self.__decode(response.content,response.headers.get('Content-Type', ''))



    @staticmethod
    def __from_binary(binary: bytes):
        return binary.decode(encoding='utf-8', errors='ignore')


    def __decode(self, content, _type=''):
        if not content:
            return ""
        if _type.find('json') >= 0:
            return json.loads(self.__from_binary(content))
        if _type.find('text') >= 0:
            return str(self.__from_binary(content))
        return content


    def __data(self, **kwargs):
        if self.headers.get('Content-Type', '').lower() == 'application/json':
            return json.dumps(kwargs.get('data', None))
        else:
            return kwargs.get('data', '')

    def get(self, *args, **kwargs):
        return self.format(super().get(self.url(*args, **kwargs)))


    def post(self, *args, **kwargs):
        return self.format(super().post(self.url(*args, **kwargs), data=self.__data(**kwargs)))


    def put(self, *args, **kwargs):
        return self.format(super().put(self.url(*args, **kwargs), data=self.__data(**kwargs)))


    def delete(self, *args, **kwargs):
        return self.format(super().delete(self.url(*args, **kwargs), data=self.__data(**kwargs)))



if __name__ == '__main__':
   s = Session('127.0.0.1:5000', '', protocol='http')
   data = {'login__': 'barkhatova5',
           'role': 'admin'}
   # resp = s.post('employees', data=data)
   # resp = s.get('employees', url_param={'order': 'idq'} )
   # resp = s.get('employees/48')
   resp = s.get('employees', url_param={'login': 'obarkhatova1'})
   print(resp)