from contextlib import contextmanager
from aenum import IntFlag
from sqlalchemy import create_engine as alch_create_engine
import yaml


class MERGED_OPT(IntFlag):
    EXTEND_LIST   = 0x01
    REPLACE_OTHER = 0x02


def db_url(settings_path):
    with open(settings_path, 'r') as f:
        settings = yaml.load(f, Loader=yaml.FullLoader)
        if settings.get('db'):
            db = settings['db']
            return f"{db.pop('login')}:{db.pop('password')}@{db.pop('ip')}/{db.pop('name')}"
        else:
            raise ValueError(f'DB settings are expected. Please refer to {settings_path}')


def create_engine(**kwargs):
    '''
    Wrapper above sqlalchemy.create_engine.
    Kwargs:
        url: url to access data base
             #todo add port value into template
             url template platform+dialect://login:password@ip/db_name
             postgres+psycopg2://qauser:qauser@localhost/highload
        platform:
        dialect:
        login:
        password:
        ip: data base address
        port:
        name: data base name
    '''
    if kwargs.get('url'):
        return alch_create_engine(kwargs.pop('url'), **kwargs)
    elif kwargs.get('settings'):
        return alch_create_engine(db_url(kwargs.pop('settings')), **kwargs)


def merge_dicts(dict1: dict, dict2: dict, opts: MERGED_OPT = MERGED_OPT.EXTEND_LIST | MERGED_OPT.REPLACE_OTHER):
    """
    Update dict1 by dict2. List values are extended if param opts
    MERGED_OPT.EXTEND_LIST, otherwise list values are replaced,
    dict values are recursively updated, other values are replaced
    (if param opt MERGED_OPT.REPLACE_OTHER).  Dict2 has higher
    priority. Return generator
    """
    for key in set(dict1) - set(dict2):
        yield key, dict1[key]
    for key in set(dict2) - set(dict1):
        yield key, dict2[key]
    for key in set(dict1) & set(dict2):
        value1 = dict1[key]
        value2 = dict2[key]
        if isinstance(value1, list) and isinstance(value2, list):
            if MERGED_OPT.EXTEND_LIST in opts:
                yield key, value1 + value2
            else:
                yield key, value2
        elif isinstance(value1, dict) and isinstance(value2, dict):
            yield key, dict(merge_dicts(value1, value2, opts))
        elif MERGED_OPT.REPLACE_OTHER in opts:
            yield key, value2
        else:
            raise TypeError('Incompatible types: value1 is {}, value2 in {}'
                            .format(type(value1), type(value2)))