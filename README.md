# Sport Compensation

### Учёт компенсации спортивных мероприятий

#### Цели
* Исключить необходимость ежемесячного оформления заявки на компенсацию. 
Предоставляя договор с спорт клубом, сотрудник подразумевает,что компенсация должна назначаться автоматически 
на весь срок договора
* Исключить возможность непреднамеренного удаления одним сотрудникос строки заявки другого
* Сократить время, затрачиваемое администратором на сверку дат договоров и опрос сотрудников

#### Функциональные требования #todo
* Роли и права доступа 
* Роль администратор, сотрудник
* Возможность загрузки\хранения скана договора с указанием даты начала и даты окончания 
* Уведомления в т.ч. на почту о приближающемся окончании срока договора
* Генерация списка сотрудников, имеющих открытые договоры на начало текущего месяца
* Возможность сохранить список на компенсацию
* Возможность отредактировать список 
* Выгрузка списка в xls формат  
* Login по LDAP


#### База данных

##### Install ansible
```buildoutcfg
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt update
sudo apt install ansible
```

```

$ ansible --version
ansible 2.9.6
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/home/obarkhatova/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/dist-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.12 (default, Oct  8 2019, 14:14:10) [GCC 5.4.0 20160609]
```

##### Install PostgreSQL
```buildoutcfg
$ cd path/to/sport/ansible
$ ansible localhost -i ~/sport/hosts -m include_role -a name=postgresql_install -e version=12
```
##### Create data base 

```buildoutcfg
$ cd path/to/sport/ansible
$ ansible localhost -i ~/hl-tool/hosts -m include_role -a name=postgresql_add_db -e db_name=sport -e db_user=qauser -e db_pass=qauser

```
##### Autogenerate the initial revision
```buildoutcfg
alembic -x settings=settings.yaml revision -m "baseline" --autogenerate 
```
##### Create tabes
```buildoutcfg
alembic -x settings=settings.yaml upgrade head
```