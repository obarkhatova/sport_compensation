* Как правильно задавать конфигурацию приложения. Из документации best practices:
>Keep a default configuration in version control. Either populate the config with this default configuration or import it in your own configuration files before overriding values.

>Use an environment variable to switch between the configurations. This can be done from outside the Python interpreter and makes development and deployment much easier because you can quickly and easily switch between different configs without having to touch the code at all. If you are working often on different projects you can even create your own script for sourcing that activates a virtualenv and exports the development configuration for you.

>Use a tool like fabric in production to push code and configurations separately to the production server(s). For some details about how to do that, head over to the Deploying with Fabric pattern.
>
Что реально используется?

* Хочется подробнее остановиться на current_app appl_context и application factoty, request context 
https://flask.palletsprojects.com/en/1.1.x/appcontext/
Не поняла как обрабатываются несколько запросов одновременно. Внутренняя кухня с push context, pop context
>Because the contexts are stacks, other contexts may be pushed to change the proxies during a request. While this is not a common pattern, it can be used in advanced applications to, for example, do internal redirects or chain different applications together.

В каких случаях могут понадобиться не объекты прокси а сами app и request живьём? См  _get_current_object()

* Про документирование API , сфинкс и прочие

* Какие ошибки принято отлавливать? 

  ** Несуществующие параметры урла, обрабатываем или пропускаем?  
  ** Параметр правильный типа order или filter, но мы по таким значениям не фильтруем, или в принципе нет столбцов в базе
  Что предпочтительнее, куча if-ов, чтобы предотвратить ошибку или возвращать exception 
  ** Неправильная схема в post
  
