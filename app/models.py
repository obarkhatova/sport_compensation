from . import db
from sqlalchemy import inspect
from sqlalchemy.dialects.postgresql import BYTEA
from sqlalchemy.ext.hybrid import hybrid_property

roles = db.Enum('admin', 'guest', name='role')


compensation = db.Table('compensations', db.Model.metadata,
    db.Column('reward_id', db.Integer, db.ForeignKey('rewards.id')),
    db.Column('contract_id', db.Integer, db.ForeignKey('contracts.id'))
)

class Common():
    @property
    def as_dict(self) -> {}:
        _dict = {}
        for key in self.__mapper__.c.keys():
            if not key.startswith('_'):
                _dict[key] = getattr(self, key)

        for key, prop in inspect(self.__class__).all_orm_descriptors.items():
            if isinstance(prop, hybrid_property):
                _dict[key] = getattr(self, key)
        return _dict


    @classmethod
    def columns(cls) -> []:
        _columns = []
        for key in cls.__mapper__.c.keys():
            if not key.startswith('_'):
                _columns.append(key)

        for key, prop in inspect(cls).all_orm_descriptors.items():
            if isinstance(prop, hybrid_property):
                _columns.append(key)
        return _columns


class Contract(db.Model, Common):
    __tablename__ = 'contracts'
    id = db.Column(db.Integer, primary_key=True)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    gym_id = db.Column(db.Integer, db.ForeignKey('gyms.id'))
    employee_id = db.Column(db.Integer, db.ForeignKey('employees.id'))
    compensations = db.relationship('Reward',
                                  secondary=compensation,
                                  backref='contracts')
    scan = db.Column(BYTEA)


class Gym(db.Model, Common):
    __tablename__ = 'gyms'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    contracts = db.relationship('Contract', backref='gyms')


class Employee(db.Model, Common):
    __tablename__ = 'employees'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(20), unique=True)
    role = db.Column('role', roles)
    contracts = db.relationship('Contract', backref='employees')


class Reward(db.Model, Common):
    __tablename__ = 'rewards'
    id = db.Column(db.Integer, primary_key=True)
    sum = db.Column(db.Float)
    date = db.Column(db.DateTime)
    contracts_rewarded = db.relationship('Contract',
                            secondary=compensation,
                            backref='rewards')

