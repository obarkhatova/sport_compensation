from dotenv import load_dotenv
from pathlib import Path
import os
import yaml
from lib.util import merge_dicts


PRJ_DIR = Path(__file__).parent.parent
SETTINGS_PATH = PRJ_DIR / 'settings.yaml'

# todo: it's an artificial way to load environment
load_dotenv(PRJ_DIR / '.env')


class Config():
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    def __init__(self):
        self.settings = None
        with open(SETTINGS_PATH, 'r') as f:
            self.settings = yaml.load(f, Loader=yaml.FullLoader)

    def _db_url(self):
        if self.settings.get('db'):
            db = self.settings['db']
            return f"{db.pop('platform')}+{db.pop('dialect')}://" \
                   f"{db.pop('login')}:{db.pop('password')}@{db.pop('ip')}/{db.pop('name')}"



class DevConfig(Config):
    def __init__(self):
        super().__init__()
        custom_settings_path = os.environ.get('SPORT_SETTINGS')
        if custom_settings_path:
            with open(custom_settings_path, 'r') as f:
                custom_settings = yaml.load(f, Loader=yaml.FullLoader)
                merged_settings = dict(merge_dicts(self.settings, custom_settings))
                self.settings = merged_settings

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return os.environ.get('DATABASE_URL') or self._db_url()