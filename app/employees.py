from flask import request, jsonify, abort
from flask import current_app as app
from flask_api import status
from flask_inputs import Inputs
from flask_inputs.validators import JsonSchema

from sqlalchemy.exc import IntegrityError, CompileError
from app.models import db, Employee
from app.errors import SchemaValidationError, DataBaseError, NotFoundError, ParamValueError

from psycopg2.errors import UniqueViolation

employee_schema = {
   'type': 'object',
   'properties': {
       'login': {
           'type': 'string',
       },
       'role': {
            'type': 'string',
            'enum': ['admin', 'guest']
       }
   },
   'required': ['login', 'role']
}

class EmployeeInputs(Inputs):
   json = [JsonSchema(schema=employee_schema)]


def validate_employee(request):
   inputs = EmployeeInputs(request)
   if not inputs.validate():
       return inputs.errors


# todo pagination
@app.route("/employees/", defaults={'id': None})
@app.route('/employees/<int:id>', methods=['GET'])
def employees(id):

    # def suitable_column(table, func='', default=''):
    #     def wrapper(requested_column):
    #         column = default
    #         if requested_column:
    #             if requested_column in table.columns():
    #                 column = requested_column
    #             else:
    #                 raise ParamValueError(func, requested_column)
    #         return column

    if id:
        empl = Employee.query.get(id)
        if not empl:
            raise NotFoundError('employee', id)
        return jsonify(empl.as_dict)
    else:
        columns = Employee.columns()
        filter = {}
        for key in columns:
            val = request.args.get(key)
            if val and val not in ['order', 'offset', 'limit']:
                filter.update({key: val})
        order_val = request.args.get('order', 'login')
        employees = []
        q = Employee.query
        try:
            for k, v in filter.items():
                q = q.filter(getattr(Employee, k) == v)
            employees = q.order_by(order_val).all()
        except CompileError as e:
             raise DataBaseError(str(e))

        return jsonify(list(map(lambda x: x.as_dict, employees)))


@app.route('/employees', methods=['POST'])
def employees_post():
    errors = validate_employee(request)
    if errors is not None:
        raise SchemaValidationError(errors)

    employee = Employee(login=request.json.get('login'),
                        role=request.json.get('role'))

    db.session.add(employee)
    try:
        db.session.commit()
    except IntegrityError as e:
        if isinstance(e.orig, UniqueViolation):
            raise DataBaseError(str(e))
        raise e

    return jsonify(employee.as_dict())




#todo trailing slash