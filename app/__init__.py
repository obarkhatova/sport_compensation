from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app.config import Config, DevConfig
from app.errors import errors
import os

db = SQLAlchemy()

def create_app(config_cls = Config):
    app = Flask(__name__)
    launch_type = os.environ.get('FLASK_ENV')
    if launch_type == 'development':
        app.config.from_object(DevConfig())

    elif launch_type == 'production':
        exit('Production version is not supported')
    else:
        exit('Launch type is undefined')

    db.init_app(app)
    for cls, handler in errors.items():
        app.register_error_handler(cls, handler)

    with app.app_context():
        from . import routes, employees

        # Create tables for our models
        db.create_all()

    return app


