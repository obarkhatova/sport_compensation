from flask import jsonify, Response
from flask_api import status

class Error(Exception):
    status_code = status.HTTP_400_BAD_REQUEST

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class SchemaValidationError(Error):
    def __init__(self, message, payload=None):
        super().__init__(message, status_code=status.HTTP_400_BAD_REQUEST, payload=payload)


class DataBaseError(Error):
    def __init__(self, message, payload=None):
        super().__init__(message, status_code=status.HTTP_400_BAD_REQUEST, payload=payload)


class NotFoundError(Exception):
    status_code = status.HTTP_404_NOT_FOUND
    def __init__(self, entity, id, payload=None):
        message = f'Not found: {entity} with id {id}'
        super().__init__(message, status_code=status.HTTP_404_NOT_FOUND, payload=payload)


class ParamValueError(Error):
    def __init__(self, param, value, payload=None):
        message = f'Unexpected param value: {param}={value}'
        super().__init__(message, status_code=status.HTTP_400_BAD_REQUEST, payload=payload)


def handle_schema_validation_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

# todo should be optimized
def handle_data_base_error(error):
    return Response(error.message, status=error.status_code)

def handle_not_found_error(error):
    return Response(error.message, status=error.status_code)

# todo some template may be returned here
def not_found_error(error):
    return Response(status=error.code, response=error.description)

def handle_param_value_error(error):
    return Response(error.message, status=error.status_code)

errors = { SchemaValidationError: handle_schema_validation_error,
           DataBaseError: handle_data_base_error,
           NotFoundError: handle_not_found_error,
           ParamValueError: handle_param_value_error,
           404: not_found_error}





