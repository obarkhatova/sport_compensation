from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Table, Integer, Float, String, DateTime, ForeignKey
from sqlalchemy.dialects.postgresql import JSONB, ENUM, BYTEA
from sqlalchemy.orm import relationship

Base = declarative_base()

roles = ENUM('admin', 'guest', name='role')


compensation = Table('compensations', Base.metadata,
    Column('reward_id', Integer, ForeignKey('rewards.id')),
    Column('employee_id', Integer, ForeignKey('employees.id')),
    Column('contract_id', Integer, ForeignKey('contracts.id'))
)


class Contract(Base):
    __tablename__ = 'contracts'
    id = Column(Integer, primary_key=True)
    start = Column(DateTime)
    end = Column(DateTime)
    gym_id = Column(Integer, ForeignKey('gyms.id'))
    employee_id = Column(Integer, ForeignKey('employees.id'))
    rewards = relationship('Reward',
                          secondary=compensation,
                          backref='contracts')
    scan = Column(BYTEA)


class Gym(Base):
    __tablename__ = 'gyms'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    contracts = relationship('Contract', backref='gyms')


class Employee(Base):
    __tablename__ = 'employees'
    id = Column(Integer, primary_key=True)
    login = Column(String(20))
    role = Column('role', roles, nullable=False)
    contracts = relationship('Contract', backref='employees')
    rewards = relationship('Reward',
                          secondary=compensation,
                          backref='employees')


class Reward(Base):
    __tablename__ = 'rewards'
    id = Column(Integer, primary_key=True)
    sum = Column(Float)
    date = Column(DateTime)
    employees = relationship('Employee',
                              secondary=compensation,
                              backref='rewards')

#todo MIGRATION is broken now